﻿/*
Each of the 6 functions below correspond to a HTML button onclick event.
1 for each object and 1 for each box.
*/
function Laptop() {
	session.service("ALMemory").then(function(ALMemory) {
        ALMemory.getData("varObject").then(function(val){
            val = "laptop";
            ALMemory.insertData("varObject", val);
        });
	}); 
}

function Bottle() {
	session.service("ALMemory").then(function(ALMemory) {
        ALMemory.getData("varObject").then(function(val){
            val = "bottle";
            ALMemory.insertData("varObject", val);
        });
	}); 
}

function Racket() {  
	session.service("ALMemory").then(function(ALMemory) {
        ALMemory.getData("varObject").then(function(val){
            val = "tennis racket";
            ALMemory.insertData("varObject", val);
        });
	}); 
}

function YellowBox() {  
	session.service("ALMemory").then(function(ALMemory) {
        ALMemory.getData("varBox").then(function(val){
            val = "yellow";
            ALMemory.insertData("varBox", val);
        });
	}); 
}

function RedBox() {  
	session.service("ALMemory").then(function(ALMemory) {
        ALMemory.getData("varBox").then(function(val){
            val = "red";
            ALMemory.insertData("varBox", val);
        });
	}); 
}

function BrownBox() {  
	session.service("ALMemory").then(function(ALMemory) {
        ALMemory.getData("varBox").then(function(val){
            val = "brown";
            ALMemory.insertData("varBox", val);
        });
	}); 
}


/*
Each of the 6 functions below correspond to a choregraphe event.
1 for each object and 1 for each box.
*/
$(document).ready(function () {
    // session = new QiSession("127.0.0.1:80");

    //  use qimessaging-json directly 
    // the port 8002 is the port qimessaging-json is listening to
    // the string "1.0" avoids to have to rewrite url with reverse proxy
    session = new QiSession("127.0.0.1:8002", "1.0");

	session.service("ALMemory").done(function(ALMemory) {
		ALMemory.subscriber("saidLaptop").done(function(subscriber) {
			subscriber.signal.connect(function(val) {
				if (val==1) {
                    val = 0;
                    Laptop();
				}
			})
		})
    })

	session.service("ALMemory").done(function(ALMemory) {
		ALMemory.subscriber("saidBottle").done(function(subscriber) {
			subscriber.signal.connect(function(val) {
				if (val==1) {
                    val = 0;
                    Bottle();
				}
			})
		})
    })
    
	session.service("ALMemory").done(function(ALMemory) {
		ALMemory.subscriber("saidTennisRacket").done(function(subscriber) {
			subscriber.signal.connect(function(val) {
				if (val==1) {
                    val = 0;
                    Racket();
				}
			})
		})
	})

	session.service("ALMemory").done(function(ALMemory) {
		ALMemory.subscriber("saidYellow").done(function(subscriber) {
			subscriber.signal.connect(function(val) {
				if (val==1) {
                    val = 0;
                    YellowBox();
				}
			})
		})
    })
    
	session.service("ALMemory").done(function(ALMemory) {
		ALMemory.subscriber("saidRed").done(function(subscriber) {
			subscriber.signal.connect(function(val) {
				if (val==1) {
                    val = 0;
                    RedBox();
				}
			})
		})
    })
    
	session.service("ALMemory").done(function(ALMemory) {
		ALMemory.subscriber("saidBrown").done(function(subscriber) {
			subscriber.signal.connect(function(val) {
				if (val==1) {
                    val = 0;
                    BrownBox();
				}
			})
		})
	})

});
