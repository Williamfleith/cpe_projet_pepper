#IMPORT-----------------------------------------------------------------

# Import module
from environment import fnc_env
from imageRecognition import fnc_imgRecognition
from tabletSimulator import fnc_tabletSimulator
from catchObject import fnc_catchobj
from speechRecognition import fnc_speechRecognition


# Import usual
import random
import math
import time

# Import for environment
import pybullet
import pybullet_data
from qibullet import PepperVirtual
from qibullet import SimulationManager

# Import for imageRecognition
import Algorithmia
import cv2
from PIL import Image

# Import for tabletSimulator
from naoqi import ALProxy


#PGRM PRINC-------------------------------------------------------------

pepper = fnc_env()

# The user has a choice:
# Uses the speech recognition or the tablet to make his choice.
choice = input("Use tablet or speech recognition? ['tablet'/'speech']: ")
if (choice == "speech"):
    varObject_script, varBox_script = fnc_speechRecognition()
elif (choice == "tablet"):
    varObject_script, varBox_script = fnc_tabletSimulator()


# These are the global variable for the object and the product
chosen_obj = varObject_script
chosen_box = varBox_script

# Set the Pepper speed at his maximum
pepper.angular_velocity = 2.0
pepper.linear_velocity = 0.55

# Lower Pepper's arms because they are on the camera frame.
# Lower Pepper's head to have a camera frame which contains all of each object.
pepper.setAngles("RShoulderPitch", 1.5, 1)
pepper.setAngles("LShoulderPitch", 1.5, 1)
pepper.setAngles("HeadPitch", 0.2, 1)

# Pepper moves until the first object position
pepper.moveTo(1.4, 0.0, (math.pi)/2, frame=PepperVirtual.FRAME_WORLD)
time.sleep(0.1)
list_obj = fnc_imgRecognition(pepper, chosen_obj)

# If the chosen object is recognized in the camera frame,
# catch it and put it in the chosen box.
if (chosen_obj in list_obj) :
    print("catch object")
    fnc_catchobj(pepper,chosen_box,chosen_obj)

# If the chosen object is not recognized in the camera frame,
else :
    # Pepper moves until the second object position
    pepper.moveTo(2.0, 0.0, (math.pi)/2, frame=PepperVirtual.FRAME_WORLD)
    time.sleep(0.1)
    list_obj = fnc_imgRecognition(pepper, chosen_obj)
    # If the chosen object is recognized in the camera frame,
    # catch it and put it in the chosen box.
    if (chosen_obj in list_obj) :
        print("catch object")
        fnc_catchobj(pepper,chosen_box, chosen_obj)

    # If the chosen object is not recognized in the camera frame,
    else :
        # Pepper moves until the third object position
        pepper.moveTo(2.6, 0.0, (math.pi)/2, frame=PepperVirtual.FRAME_WORLD)
        time.sleep(0.1)
        list_obj = fnc_imgRecognition(pepper, chosen_obj)

        # If the chosen object is recognized in the camera frame,
        # catch it and put it in the chosen box.
        if (chosen_obj in list_obj) :
            print("catch object")
            fnc_catchobj(pepper,chosen_box, chosen_obj)




while True:
    pass
simulation_manager.stopSimulation(client)