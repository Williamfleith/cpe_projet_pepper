import Algorithmia
import cv2
from qibullet import SimulationManager
from qibullet import PepperVirtual
import time

def WorkOnAlgorithmiaOutput(input) :
    """
    Aim:
        Work on the Algorithmia return data to keep what is useful in the code.
        This function is called as last instruction of the function below.
    Input:
        input (dict): The data return by Algorithmia.
    Output:
        output (list): A list containing all the object recognize by Algorithmia
        on the Pepper's top camera frame.

    """
    data = input.get('boxes')
    output = []
    for i in range(len(data)):
        obj = data[i].get('label')
        output.append(str(obj))

    return output


def fnc_imgRecognition(pepperInstance, choosen_obj) :
    """
    Aim:
        Allows the image recognition on the Pepper top camera.
    Input:
        pepperInstance (instance): The instance of Pepper created in the main.py.
        choosen_obj (str): The object chosen by the user
    Output:
        list_obj (list): A list containing all the object recognize by Algorithmia
        on the Pepper's top camera frame.

    """
    handle = pepperInstance.subscribeCamera(PepperVirtual.ID_CAMERA_TOP)
    
    img = pepperInstance.getCameraFrame()
    cv2.imshow("synthetic top camera", img)
    cv2.waitKey(1)
    cv2.imwrite('resultat.png', img)

    text_file = "data://.algo/deeplearning/ObjectDetectionCOCO/temp/IMG_tmp.png"
    clientA = Algorithmia.client('simvjFctAleu25cnuHa1x9zVWrJ1')
    time.sleep(0.1)
    clientA.file(text_file).putFile("resultat.png")

    input = {
        "image" : text_file,
        "output" : "data://.algo/deeplearning/ObjectDetectionCOCO/temp/IMG_tmp_annoted.png",
        "min_score" : 0.26,
        "model" : "ssd_mobilenet_v1"
    }

    
    algo = clientA.algo('deeplearning/ObjectDetectionCOCO/0.2.1')
    algo.set_options(timeout=300)
    res = algo.pipe(input).result
    localfile = clientA.file("data://.algo/deeplearning/ObjectDetectionCOCO/temp/IMG_tmp_annoted.png")
    
    list_obj = WorkOnAlgorithmiaOutput(res)
    print(list_obj)
    return list_obj