# -*- coding: utf-8 -*-
""" 

created by William FLEITH
on 02/04/2020

"""
import time
import random
import math
import pybullet
import pybullet_data
from qibullet import PepperVirtual
from qibullet import SimulationManager


def fnc_env():
    simulation_manager = SimulationManager()
    client = simulation_manager.launchSimulation(gui=True)
    pepper = simulation_manager.spawnPepper(
        client,
        translation=[-0.75,0.0,0],
        quaternion=[0,0,0,1],
        spawn_ground_plane=False)

    pybullet.setAdditionalSearchPath(pybullet_data.getDataPath())


    #pybullet objects spawning randomly
    position = [1.4,2,2.6]
    time.sleep(0.1)
    positionRand = random.sample(position,3)

    # ----MAIN ENVIRONMENT---------------------------------------

    pybullet.loadURDF(
        "urdf/table/table.urdf",
        basePosition=[2, 1, 0],
        globalScaling=1.2,
        physicsClientId=client)

    pybullet.loadURDF(
        "urdf/bottle/totem_bottle.urdf",
        basePosition=[positionRand[0], 0.6, 0.75],
        globalScaling=1,
        physicsClientId=client)
    
    pybullet.loadURDF(
        "urdf/tennis/totem_tennis.urdf",
        basePosition=[positionRand[1], 0.6, 0.75],  
        globalScaling=1,
        physicsClientId=client)

    # hold tennis racket droit
    pybullet.loadURDF(
        "urdf/invbox/box.urdf",
        basePosition=[positionRand[1], 0.67, 0.75],
        globalScaling=0.3,
        physicsClientId=client)

    pybullet.loadURDF(
        "urdf/laptop/laptop.urdf",
        basePosition=[positionRand[2], 0.5, 0.8], 
        globalScaling=1,
        physicsClientId=client)

    # Hold laptop
    pybullet.loadURDF(
        "urdf/minibox/box.urdf",
        basePosition=[positionRand[2], 0.5, 0.67], 
        globalScaling=1,
        physicsClientId=client)



    # ----CONTAINERS---------------------------------------------

    pybullet.loadURDF(
        "urdf/redbox/box.urdf",
        basePosition=[1, -2, 0],
        globalScaling=2,
        physicsClientId=client)

    pybullet.loadURDF(
        "urdf/cardboardbox/cardboardbox.urdf",
        basePosition=[2, -2, 0.1],
        globalScaling=2,
        physicsClientId=client)

    pybullet.loadURDF(
        "urdf/yellowbox/box.urdf",
        basePosition=[3, -2, 0],
        globalScaling=2,
        physicsClientId=client)



    # ----GLOBAL FANCY ENVIRONMENT------------------------------- 

    pybullet.loadURDF(
        "urdf/bar/bar.urdf",
        basePosition=[-1.5, -0.5, 0.5],
        globalScaling=0.7,
        physicsClientId=client)

    pybullet.loadURDF(
        "urdf/fork/fork.urdf",
        basePosition=[-1.5, -0.7, 0.7],
        globalScaling=1.2,
        physicsClientId=client)

    pybullet.loadURDF(
        "urdf/spoon/spoon.urdf",
        basePosition=[-1.5, -0.5, 0.815],
        globalScaling=1.2,
        physicsClientId=client)
    
    # Generate the ground for the entire room
    for i in range(5):
        for j in range(5):

            pybullet.loadURDF(
                "urdf/sol/sol.urdf",
                basePosition=[i, j, 0.01],
                globalScaling=0.5,
                physicsClientId=client)
      
            pybullet.loadURDF(
                "urdf/sol/sol.urdf",
                basePosition=[-i, -j, 0.01],
                globalScaling=0.5,
                physicsClientId=client)
            pybullet.loadURDF(
                "urdf/sol/sol.urdf",
                basePosition=[i, -j, 0.01],
                globalScaling=0.5,
                physicsClientId=client)
            pybullet.loadURDF(
                "urdf/sol/sol.urdf",
                basePosition=[-i, j, 0.01],
                globalScaling=0.5,
                physicsClientId=client)

    

    pybullet.loadURDF(
        "urdf/wallleft/wall.urdf",
        basePosition=[0, -4, 2],
        globalScaling=4,
        physicsClientId=client)
    
    pybullet.loadURDF(
        "urdf/wallright/wall.urdf",
        basePosition=[0, 4, 2],
        globalScaling=4,
        physicsClientId=client)
    
    pybullet.loadURDF(
        "urdf/wallback/wall.urdf",
        basePosition=[-3.5, 0, 2],
        globalScaling=4,
        physicsClientId=client)

    pybullet.loadURDF(
        "urdf/cactus/cactus.urdf",
        basePosition=[-0.5, -2, 0],
        globalScaling=1.5,
        physicsClientId=client)

    pybullet.loadURDF(
        "urdf/cactus/cactus.urdf",
        basePosition=[-0.5, 2, 0],
        globalScaling=1.5,
        physicsClientId=client)
    
    pybullet.loadURDF(
        "urdf/plant/plant.urdf",
        basePosition=[1.5, 3, 0],
        globalScaling=2,
        physicsClientId=client)

    pybullet.loadURDF(
        "urdf/plant/plant.urdf",
        basePosition=[3, 3, 0],
        globalScaling=2,
        physicsClientId=client)
    
    
    return pepper