# Robotics Project - All rights reserved to CPE Lyon 

## Overview

__Team member names:__

Etienne ENGEL, William FLEITH

__Youtube links:__

https://www.youtube.com/watch?v=tvy7RFq7qjs

__Sources:__

[A GitHub which explains the project](https://github.com/cpe-majeure-robotique/S8-Simulated-Pepper-Project)

[A GitLab which explains how to use the tablet simulator](https://gitlab.com/davidvivier/naoqi-tablet-simulator)

__Bonus numbers with their status:__

6 done / 8 done / 10 done

## Context
This repository contains the final version of our Pepper Robot scenario with:
1. A simulated world;
1. A Pepper robot;
1. A main table with:
   1. 3 recognizable objects among the [coco dataset](http://cocodataset.org/#explore);
   1. 3 known locations were to place objects;
   1. A random attribution of the objects on the 3 locations;
1. 3 known and different storage locations;
1. A Human-Robot Interface (tablet + .top + speech recognition).

Our Pepper Robot scenario is:
1. Move in front of the first known position;
1. Take a picture of an object;
1. Send the picture to [the coco service of Algorithmia](https://algorithmia.com/algorithms/deeplearning/ObjectDetectionCOCO);
1. Get back the Algorithmia information and extract a list of recognize objects;
1. Verify if the object ask by the user is in the list of recognize objects;
1. If yes:
   1. Grab the object;
   1. Move to the storage location asked by the user;
   1. Release the object in the storage location.
1. If no:
   1. Move to the next known position;
   1. Repeat steps since the 2nd one.

## Explanation of contents

### main.py

----

The file [`main.py`](https://gitlab.com/Projets_4ETI_S8_1920/Sujet_1__Simulated_Pepper_Project/g1_engel_fleith/-/blob/dev/main.py) contains all the call to the functions store in the other `.py` files.
For run our Pepper Robot scenario, it is this file which has to be run.

### Other `.py` files

----

Each `.py` file contains a function useful for the scenario:
1. [`environment.py`](https://gitlab.com/Projets_4ETI_S8_1920/Sujet_1__Simulated_Pepper_Project/g1_engel_fleith/-/blob/dev/environment.py) contains a function to load the simulated world;
1. [`imageRecognition.py`](https://gitlab.com/Projets_4ETI_S8_1920/Sujet_1__Simulated_Pepper_Project/g1_engel_fleith/-/blob/dev/imageRecognition.py) contains a function
to send an image to Algorithmia and get back the useful information;
1. [`catchObject.py`](https://gitlab.com/Projets_4ETI_S8_1920/Sujet_1__Simulated_Pepper_Project/g1_engel_fleith/-/blob/dev/catchObject.py) contains a function
which allows to catch the object asked by the user and drop it in
the storage asked by the user;
1. [`speechRecognition.py`](https://gitlab.com/Projets_4ETI_S8_1920/Sujet_1__Simulated_Pepper_Project/g1_engel_fleith/-/blob/dev/speechRecognition.py) contains a function
which allows the speech recognition to choose the object and the storage by speaking in a microphone;
1. [`tabletSimulator.py`](https://gitlab.com/Projets_4ETI_S8_1920/Sujet_1__Simulated_Pepper_Project/g1_engel_fleith/-/blob/dev/tabletSimulator.py) contains a function
wich allows to choose the object and the storage by using a web site;

### Other folders

----

Each folder contains useful data  for the functions above:

1. [urdf](https://gitlab.com/Projets_4ETI_S8_1920/Sujet_1__Simulated_Pepper_Project/g1_engel_fleith/-/tree/dev/urdf)
contains files to load the environment objects;
1. [apps](https://gitlab.com/Projets_4ETI_S8_1920/Sujet_1__Simulated_Pepper_Project/g1_engel_fleith/-/tree/dev/apps/SimpleWebPython)
contains html, js, .top and other files for the HRI;
1. [libqi-js_v2](https://gitlab.com/Projets_4ETI_S8_1920/Sujet_1__Simulated_Pepper_Project/g1_engel_fleith/-/tree/dev/libqi-js_v2)
contains files needed for the code communication for the HRI;
1. [naoqi-tablet-simulator_v2](https://gitlab.com/Projets_4ETI_S8_1920/Sujet_1__Simulated_Pepper_Project/g1_engel_fleith/-/tree/dev/naoqi-tablet-simulator_v2)
contains files needed to launch the Pepper tablet simulator;

## How to use our project

If you want to use our code in the same condition,
you must run it in our venv by writing in a terminal 

`source /home/tp/softbankRobotics/venv/bin/activate`

You must adapt the path until the venv folder.

If you want to use our code in the same condition but without our venv, 
you have all the packages with their version in the file `requirements.txt`.

To install the requirements.txt version you must run:

`pip install -r requirements.txt`

## How to launch a simulation

For each simulation the user can choose between 3 human robot interfaces:
1. Web site
1. Choregraphe dialog
1. Speech recognition

### Web site

----

To use the web site interface, do the following tasks (one terminal per command):
1. naoqi-bin
1. ./launcher.sh
1. reset web page
1. python app.py
1. python main.py
1. after the simulation launching has finished, write 'tablet'
1. choose your object and stockroom in the web site
	
### Choregraphe launcher

----

To use the choregraphe interface, do the following tasks (one terminal per command):
1. naoqi-bin
1. ./launcher.sh
1. reset web page
1. python app.py
1. python main.py
1. after the simulation launching has finished, write 'tablet'
1. choregraphe_launcher
1. connect to the pepper robot with port 9559
1. write your object and stockroom in the dialog box

### Speech recognition

----

To use the speech recognition interface, do the following tasks (one terminal per command):
1. naoqi-bin
1. python main.py
1. after the simulation launching has finished, write 'speech'
1. say the object you want, check if it is the right one
1. say the stockroom you want, check if it is the right one
