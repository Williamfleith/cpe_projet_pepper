import speech_recognition as sr


def fnc_speechRecognition():
    """
    Aim:
        Allows the speech recognition on 1 english word at a time.
    Input:
        Nothing.
    Output:
        var_object (str): The object word, without space at the beginning and at the and, in lowercase.
        var_box (str): The box word, without space at the beginning and at the and, in lowercase.

    """
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Say which object do you want")
        audio = r.listen(source)

    try:
        text = r.recognize_google(audio)
        print("You said : " + text)
    except sr.UnknownValueError:
        print("Audio pas compris")
    except sr.RequestError as e:
        print("Le service Google Speech API ne fonctionne plus" + format(e))

    var_object = str(text)
    var_object = var_object.strip()
    var_object = var_object.lower()


    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Say which stockroom do you want")
        audio = r.listen(source)

    try:
        text = r.recognize_google(audio)
        print("You said : " + text)
    except sr.UnknownValueError:
        print("Audio pas compris")
    except sr.RequestError as e:
        print("Le service Google Speech API ne fonctionne plus" + format(e))

    var_box = str(text)
    var_box = var_box.strip()
    var_box = var_box.lower()

    return var_object, var_box