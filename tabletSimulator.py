from naoqi import ALProxy


def fnc_tabletSimulator():
    """
    Aim:
        Allows a web site to modify variables in the main.py code.
    Input:
        Nothing
    Output:
        varObject (str): The object word, as define in the JavaScript functions.
        varBox (str): The box word, as define in the JavaScript functions.

    """
    # create proxy on ALMemory
    memProxy = ALProxy("ALMemory","localhost",9559)

    #insertData. Value can be int, float, list, string
    memProxy.insertData("varObject", "")
    memProxy.insertData("varBox", "")
    varObject = ""
    varBox = ""

    #getData
    while (varObject=="" or varBox==""):
        varObject = memProxy.getData("varObject")
        varBox = memProxy.getData("varBox")
    return varObject, varBox