# -*- coding: utf-8 -*-
""" 

created by William FLEITH
on 09/04/2020

last update : 14/04/2020
"""
#library import 
import time
from qibullet import PepperVirtual
import math


def fnc_catchobj(pepperinst, chosen_box, chosen_obj):
	"""
	fnc_catchobj, will do the movment to catch one of the 3 object on the table

	function argument : instance of pepper, the object and the box chosen by the user.

	Depending on which box and object that are chosen pepper will go to the corresponding object (pepper.moveTo) and
	grab it with different pepper.setAngles() for his body positions
	"""

	print (chosen_box)
	print (chosen_obj)

	if chosen_obj == 'bottle':
		# Setup right arm
		pepperinst.setAngles("RShoulderPitch", 0.3, 0.3)
		pepperinst.setAngles("RElbowYaw", 1.5, 0.3)
		pepperinst.setAngles("RElbowRoll", 0.26, 0.3)
		pepperinst.setAngles("RHand", 2.1, 0.3)
		# Setup left arm
		pepperinst.setAngles("LShoulderPitch", 0.3, 0.3)
		pepperinst.setAngles("LElbowYaw", -1.5, 0.3)
		pepperinst.setAngles("LElbowRoll", -0.26, 0.3)
		pepperinst.setAngles("LHand", 2.1, 0.3)
		time.sleep(0.1)
		# Go to the good position and grab the object
		pepperinst.moveTo(0.275, 0, 0.0, frame=PepperVirtual.FRAME_ROBOT)
		pepperinst.setAngles("HipPitch", -0.2, 0.3)
		time.sleep(0.05)
		pepperinst.setAngles("RElbowRoll", 0.7, 0.1)
		pepperinst.setAngles("RElbowYaw", 0.6, 0.1)
		pepperinst.setAngles("LElbowRoll", -0.7, 0.1)
		pepperinst.setAngles("LElbowYaw", -0.6, 0.1)
		#raise his arms
		pepperinst.setAngles('LShoulderPitch', 0.3, 1.0)
		pepperinst.setAngles('RShoulderPitch', 0.3, 1.0)
		

	if chosen_obj == 'tennis racket':
		# Setup right arm
		pepperinst.setAngles("RShoulderPitch", 0.3, 0.3)
		pepperinst.setAngles("RElbowYaw", 1.5, 0.3)
		pepperinst.setAngles("RElbowRoll", 0.26, 0.3)
		pepperinst.setAngles("RHand", 2.1, 0.3)
		# Setup left arm
		pepperinst.setAngles("LShoulderPitch", 0.3, 0.3)
		pepperinst.setAngles("LElbowYaw", -1.5, 0.3)
		pepperinst.setAngles("LElbowRoll", -0.26, 0.3)
		pepperinst.setAngles("LHand", 2.1, 0.3)
		time.sleep(0.1)
		# Go to good the position and grab the object
		pepperinst.moveTo(0.285, 0, 0.0, frame=PepperVirtual.FRAME_ROBOT)
		time.sleep(0.05)
		pepperinst.setAngles("RElbowRoll", 0.8, 0.1)
		pepperinst.setAngles("RElbowYaw", 0.6, 0.1)
		pepperinst.setAngles("LElbowRoll", -0.8, 0.1)
		pepperinst.setAngles("LElbowYaw", -0.6, 0.1)


	if chosen_obj == 'laptop':
		pepperinst.setAngles('LShoulderPitch', 0.5, 1.0)
		pepperinst.setAngles('RShoulderPitch', 0.5, 1.0)

		pepperinst.setAngles('LShoulderRoll', 0.15, 1.0)
		pepperinst.setAngles('RShoulderRoll', -0.15, 1.0)

		pepperinst.setAngles('LElbowRoll', -0.421, 1.0)
		pepperinst.setAngles('RElbowRoll', 0.421, 1.0)

		pepperinst.setAngles('LElbowYaw', -1, 1.0)
		pepperinst.setAngles('RElbowYaw', 1, 1.0)

		pepperinst.setAngles('LWristYaw', -1.6, 1.0)
		pepperinst.setAngles('RWristYaw', 1.6, 1.0)

		pepperinst.moveTo(0.285, 0, 0.0, frame=PepperVirtual.FRAME_ROBOT)
		time.sleep(0.05)

		pepperinst.setAngles('LShoulderPitch', 0.3, 1.0)
		pepperinst.setAngles('RShoulderPitch', 0.3, 1.0)


	# To keep from entering in the following condition before ending previous 
	time.sleep(1)
	
	if chosen_box == 'red':

		#Go back to put the object in the corresponding box
		# pepperinst.setAngles("RShoulderPitch", 0.0, 0.1)
		pepperinst.moveTo(1, -1.5,-(math.pi)/2, frame=PepperVirtual.FRAME_WORLD,_async=False)
		#open his right hand to let the object fall in the box	
		time.sleep(0.1)
		pepperinst.setAngles("HipPitch", -0.3, 0.3)
		pepperinst.setAngles("RElbowYaw", 1.5, 0.3)
		pepperinst.setAngles("RElbowRoll", 0.26, 0.3)
		pepperinst.setAngles("LElbowYaw", -1.5, 0.3)
		pepperinst.setAngles("LElbowRoll", -0.26, 0.3)
		time.sleep(0.1)
		pepperinst.setAngles("RWristYaw", -1.5, 0.3)	

	if chosen_box == 'brown':

		#Go back to put the object in the corresponding box
		pepperinst.moveTo(2, -1.45,-(math.pi)/2, frame=PepperVirtual.FRAME_WORLD,_async=False)
		#open his right hand to let the object fall in the box	
		time.sleep(0.1)
		pepperinst.setAngles("HipPitch", -0.3, 0.3)
		pepperinst.setAngles("RElbowYaw", 1.5, 0.3)
		pepperinst.setAngles("RElbowRoll", 0.26, 0.3)
		pepperinst.setAngles("LElbowYaw", -1.5, 0.3)
		pepperinst.setAngles("LElbowRoll", -0.26, 0.3)
		time.sleep(0.1)
		pepperinst.setAngles("RWristYaw", -1.5, 0.3)	

	if chosen_box == 'yellow':

		#Go back to put the object in the corresponding box
		pepperinst.moveTo(3, -1.47,-(math.pi)/2, frame=PepperVirtual.FRAME_WORLD,_async=False)
		#open his right hand to let the object fall in the box	
		time.sleep(0.1)
		pepperinst.setAngles("HipPitch", -0.3, 0.3)
		pepperinst.setAngles("RElbowYaw", 1.5, 0.3)
		pepperinst.setAngles("RElbowRoll", 0.26, 0.3)
		pepperinst.setAngles("LElbowYaw", -1.5, 0.3)
		pepperinst.setAngles("LElbowRoll", -0.26, 0.3)
		time.sleep(0.1)
		pepperinst.setAngles("RWristYaw", -1.5, 0.3)
